-- -------------- VERIFICAR LOGIN DEL USUARIO ---------------------

DROP PROCEDURE IF EXISTS pa_verificar_user;
DELIMITER $$
CREATE PROCEDURE pa_verificar_user(nomUser VARCHAR(50), pass VARCHAR(50))
BEGIN
	DECLARE cant INT DEFAULT 0;
    DECLARE ntraUsu INT DEFAULT 0;
    DECLARE passReal VARCHAR(30) DEFAULT '';
    DECLARE respuesta INT DEFAULT 0;
    DECLARE mensaje VARCHAR(20);
    DECLARE nombre VARCHAr(30);
    DECLARE aux INT DEFAULT 0;
    DECLARE perfil VARCHAR(20) DEFAULT '';
    DECLARE dniPersona INT DEFAULT 0;
    
    SET cant = (SELECT COUNT(IdUsuario) FROM tblusuario WHERE Usuario = nomUser AND Estado = 1 AND MarcaBaja = 0);
    
    IF cant > 0 THEN
        BEGIN
            SET ntraUsu = (SELECT IdUsuario FROM tblusuario WHERE Usuario = nomUser AND Estado = 1 AND MarcaBaja = 0);
            SET passReal = (SELECT AES_DECRYPT(Contrasenia,'AES') FROM tblusuario WHERE IdUsuario = ntraUsu);
            SET aux = (SELECT pass LIKE BINARY passReal);
            
            IF aux > 0 THEN
                BEGIN
                    SELECT p.NumeroDocumento, p.Nombres
                    INTO dniPersona, nombre
                    FROM tblusuario u INNER JOIN tblpersona p ON u.IdPersona = p.IdPersona WHERE u.IdUsuario = ntraUsu;
                    
                    SET dniPersona = (SELECT p.NumeroDocumento FROM tblusuario u INNER JOIN tblpersona p ON u.IdPersona = p.IdPersona WHERE u.IdUsuario = ntraUsu);
                    SET perfil = (SELECT p.Descripcion FROM tblusuario u INNER JOIN tblperfil p ON u.IdPerfil = p.IdPerfil WHERE u.IdUsuario = ntraUsu);
                    SET mensaje = 'ACCESO CORRECTO';
                    SET respuesta = 505;
                END;
            ELSE
                SET mensaje = 'CLAVE INCORRECTA';
                SET respuesta = 501;
            END IF;
        END; 
        ELSE
            SET mensaje = 'USUARIO NO EXISTE';
            SET respuesta = 500;
    END IF;
    
    
    SELECT ntraUsu as ntraUsuario, nombre, pass as password,  respuesta, mensaje, perfil, dniPersona;
END
$$
DELIMITER ;

-- -------------- LISTAR PARAMETROS ---------------------

DROP PROCEDURE IF EXISTS pa_listar_parametros_x_flag;
DELIMITER $$
CREATE PROCEDURE pa_listar_parametros_x_flag(flag INT)
BEGIN
    
    CASE flag
        -- Tipos de documento
        WHEN 1 THEN
            SELECT Correlativo AS Codigo, Descripcion1 AS Descripcion FROM tblParametros WHERE Prefijo = 1 AND Estado = 1 AND Correlativo > 0;

        -- Rangos de Sueldo
        WHEN 2 THEN
            SELECT Correlativo AS Codigo, Descripcion1 AS Descripcion FROM tblParametros WHERE Prefijo = 2 AND Estado = 1 AND Correlativo > 0;

    END CASE;
END
$$
DELIMITER ;

-- -------------
-- ** EMPRESA **
-- -------------

-- -------------- LISTAR EMPRESAS ---------------------

DROP PROCEDURE IF EXISTS pa_listar_empresas_probas;
DELIMITER $$
CREATE PROCEDURE pa_listar_empresas_probas()
BEGIN
   SELECT IdEmpresa, CodigoEmpresaPS, RazonSocial, Ruc, Direccion, Representante1, Area1, Representante2, Area2
   FROM tblempresa 
   WHERE MarcaBaja = 0;
END
$$
DELIMITER ;

-- ------------- REGISTRAR EMPRESA  ---------------------

DROP PROCEDURE IF EXISTS pa_registrar_empresa_probas;
DELIMITER $$
CREATE PROCEDURE pa_registrar_empresa_probas(codigoTiendaPS INT, nombreEmpresa VARCHAR(100), usuario VARCHAR(20))
BEGIN
	
    INSERT INTO tblEmpresa (CodigoEmpresaPS, RazonSocial, FechaProceso, HoraProceso, UsuarioRegistro) 
    VALUES (codigoTiendaPS, nombreEmpresa, CURDATE(), CURTIME(), usuario);
	
	SELECT LAST_INSERT_ID() as IdEmpresa;
	
END$$
DELIMITER ;

-- ------------- ACTUALIZAR EMPRESA ---------------------

DROP PROCEDURE IF EXISTS pa_actualizar_empresa_probas;
DELIMITER $$
CREATE PROCEDURE pa_actualizar_empresa_probas(flag INT, id INT, razonSocial VARCHAR(100), ruc VARCHAR(11), direccion VARCHAR(100), representante1 VARCHAR(100), area1 VARCHAR(100), representante2 VARCHAR(100), area2 VARCHAR(100))
BEGIN

    CASE flag
        -- Desde Plataforma de Gestion
        WHEN 0 THEN
            UPDATE tblempresa
            SET
                RazonSocial = razonSocial,
                Ruc = ruc,
                Direccion = direccion,
                Representante1 = representante1,
                Area1 = area1,
                Representante2 = representante2,
                Area2 = area2
            WHERE IdEmpresa = id;  
        -- Desde PS
        WHEN 1 THEN
            UPDATE tblempresa
            SET RazonSocial = razonSocial
            WHERE CodigoEmpresaPS = id;  
    END CASE;
END
$$
DELIMITER ;

-- ------------- ELIMINAR EMPRESA ---------------------

DROP PROCEDURE IF EXISTS pa_eliminar_empresa_probas;
DELIMITER $$
CREATE PROCEDURE pa_eliminar_empresa_probas(flag INT, id INT)
BEGIN
    
    CASE flag
        -- Desde Plataforma de Gestion
        WHEN 0 THEN
            UPDATE tblempresa 
            SET MarcaBaja = 9
            WHERE IdEmpresa = id;
        -- Desde PS
        WHEN 1 THEN
            UPDATE tblempresa 
            SET MarcaBaja = 9
            WHERE CodigoEmpresaPS = id;
    END CASE;
END
$$
DELIMITER ;

-- -----------------
-- ** COLABORADOR **
-- -----------------

-- -------------- LISTAR COLABORADORES ---------------------

DROP PROCEDURE IF EXISTS pa_listar_colaboradores_probas;
DELIMITER $$
CREATE PROCEDURE pa_listar_colaboradores_probas(idEmpresa INT, numeroDocumento VARCHAR(15), nombres VARCHAR(50))
BEGIN
    DECLARE consulta VARCHAR(5000);

    SET consulta = 
        "SELECT c.IdColaborador, c.CodigoColaboradorPS, e.CodigoEmpresaPS, e.RazonSocial, c.TipoDocumento, p.Descripcion1 AS DescripcionTipoDocumento, c.NumeroDocumento, 
                c.Nombres, CONCAT(c.ApellidoPaterno,' ',c.ApellidoMaterno) AS Apellidos, c.ApellidoPaterno, c.ApellidoMaterno, c.Correo, c.Contrasenia, c.FechaIngreso, 
                c.Salario, r.Descripcion1 AS DescripcionSalario, c.PuestoLaboral, c.UbicacionGeografica, c.Sede
        FROM tblColaborador c 
        INNER JOIN tblEmpresa e ON c.IdEmpresa = e.IdEmpresa 
        INNER JOIN tblParametros p ON c.TipoDocumento = p.Correlativo 
        INNER JOIN tblParametros r ON c.Salario = r.Correlativo
        WHERE c.MarcaBaja = 0
        AND p.Prefijo = 1
        AND r.Prefijo = 2";

    IF idEmpresa > 0 THEN
        BEGIN
            SET consulta = CONCAT(consulta, ' AND c.IdEmpresa = ', idEmpresa);
        END;
    END IF;

    IF numeroDocumento != '' THEN
        BEGIN
            SET consulta = CONCAT(consulta, ' AND c.numeroDocumento LIKE ', "'%", numeroDocumento, "%'");
        END;
    END IF;
    IF nombres != '' THEN
        BEGIN
            SET consulta = CONCAT(consulta, ' AND c.Nombres LIKE ', "'%", nombres, "%'");
        END;
    END IF;   

    SET @query = consulta;

    PREPARE strsql FROM @query;
    EXECUTE strsql;
END
$$
DELIMITER ;

-- ------------- ACTUALIZAR COLABORADOR ---------------------

DROP PROCEDURE IF EXISTS pa_actualizar_colaborador_probas;
DELIMITER $$
CREATE PROCEDURE pa_actualizar_colaborador_probas(flag INT, id INT, tipoDocumento INT, numeroDocumento VARCHAR(15), nombres VARCHAR(50), apellidoPaterno VARCHAR(40), apellidoMaterno VARCHAR(40), correo VARCHAR(60), fechaIngreso DATE, salario INT, puestoLaboral VARCHAR(50), ubicacionGeografica VARCHAR(50), sede VARCHAR(50))
BEGIN
    DECLARE consulta VARCHAR(500);
    SET consulta = "UPDATE tblColaborador SET ";

    CASE flag
        -- Desde Plataforma de Gestion
        WHEN 0 THEN
            SET consulta = CONCAT(consulta, "TipoDocumento = ", tipoDocumento, ", ");
            SET consulta = CONCAT(consulta, "NumeroDocumento = '", numeroDocumento, "', ");
            SET consulta = CONCAT(consulta, "ApellidoPaterno = '", apellidoPaterno, "', ");
            SET consulta = CONCAT(consulta, "ApellidoMaterno = '", apellidoMaterno, "', ");
            SET consulta = CONCAT(consulta, "Nombres = '", nombres, "', ");
            SET consulta = CONCAT(consulta, "Correo = '", correo, "', ");
            SET consulta = CONCAT(consulta, "FechaIngreso = '", fechaIngreso, "', ");
            SET consulta = CONCAT(consulta, "Salario = ", salario, ", ");
            SET consulta = CONCAT(consulta, "PuestoLaboral = '", puestoLaboral, "', ");
            SET consulta = CONCAT(consulta, "UbicacionGeografica = '", ubicacionGeografica, "', ");
            SET consulta = CONCAT(consulta, "Sede = '", sede, "' ");
            SET consulta = CONCAT(consulta, "WHERE IdColaborador = ", id);
        -- Desde PS
        WHEN 1 THEN
            SET consulta = CONCAT(consulta, "ApellidoPaterno = '", apellidoPaterno, "', ");
            SET consulta = CONCAT(consulta, "ApellidoMaterno = '", apellidoMaterno, "', ");
            SET consulta = CONCAT(consulta, "Nombres = '", nombres, "', ");
            SET consulta = CONCAT(consulta, "Correo = '", correo, "' ");
            SET consulta = CONCAT(consulta, "WHERE CodigoColaboradorPS = ", id);
        -- Desde Carga Masiva
        WHEN 2 THEN
            SET consulta = CONCAT(consulta, "TipoDocumento = ", tipoDocumento, ", ");
            SET consulta = CONCAT(consulta, "NumeroDocumento = '", numeroDocumento, "', ");
            SET consulta = CONCAT(consulta, "ApellidoPaterno = '", apellidoPaterno, "', ");
            SET consulta = CONCAT(consulta, "ApellidoMaterno = '", apellidoMaterno, "', ");
            SET consulta = CONCAT(consulta, "Nombres = '", nombres, "', ");
            SET consulta = CONCAT(consulta, "Correo = '", correo, "', ");
            SET consulta = CONCAT(consulta, "FechaIngreso = '", fechaIngreso, "', ");
            SET consulta = CONCAT(consulta, "Salario = ", salario, ", ");
            SET consulta = CONCAT(consulta, "PuestoLaboral = '", puestoLaboral, "', ");
            SET consulta = CONCAT(consulta, "UbicacionGeografica = '", ubicacionGeografica, "', ");
            SET consulta = CONCAT(consulta, "Sede = '", sede, "' ");
            SET consulta = CONCAT(consulta, "WHERE NumeroDocumento = '", numeroDocumento,"'");
        -- Actualizar ID de Prestashop
        WHEN 3 THEN
            SET consulta = CONCAT(consulta, "CodigoColaboradorPS = ", id);
            SET consulta = CONCAT(consulta, " WHERE NumeroDocumento = '", numeroDocumento,"'");
    END CASE;
    
    SET @query = consulta;

    PREPARE strsql FROM @query;
    EXECUTE strsql;
END
$$
DELIMITER ;

-- ------------- ELIMINAR COLABORADOR ---------------------

DROP PROCEDURE IF EXISTS pa_eliminar_colaborador_probas;
DELIMITER $$
CREATE PROCEDURE pa_eliminar_colaborador_probas(flag INT, id INT, numeroDocumento VARCHAR(15))
BEGIN
    DECLARE consulta VARCHAR(500);
    SET consulta = "UPDATE tblColaborador SET MarcaBaja = 9 ";

    CASE flag
        -- Desde Plataforma de Gestion
        WHEN 0 THEN
            SET consulta = CONCAT(consulta, "WHERE IdColaborador = ", id);
        -- Desde PS
        WHEN 1 THEN
            SET consulta = CONCAT(consulta, "WHERE CodigoColaboradorPS = ", id);
        -- Desde Carga Masiva
        WHEN 2 THEN
            SET consulta = CONCAT(consulta, "WHERE NumeroDocumento = '", numeroDocumento, "'");
    END CASE;

    SET @query = consulta;

    PREPARE strsql FROM @query;
    EXECUTE strsql;
END
$$
DELIMITER ;

-- ------------- REGISTRAR COLABORADOR  ---------------------

DROP PROCEDURE IF EXISTS pa_registrar_colaborador_probas;
DELIMITER $$
CREATE PROCEDURE pa_registrar_colaborador_probas(idEmpresa INT, codigoColaboradorPS INT, tipoDocumento INT, numeroDocumento VARCHAR(15), nombres VARCHAR(50), apellidoPaterno VARCHAR(40), apellidoMaterno VARCHAR(40), correo VARCHAR(60), contrasenia VARCHAR(50), fechaIngreso DATE, salario INT, puestoLaboral VARCHAR(50), ubicacionGeografica VARCHAR(50), sede VARCHAR(50), usuario VARCHAR(20))
BEGIN
	
    INSERT INTO tblColaborador (CodigoColaboradorPS, TipoDocumento, NumeroDocumento, Nombres, ApellidoPaterno, ApellidoMaterno, Correo, Contrasenia, FechaIngreso, Salario, PuestoLaboral, UbicacionGeografica, Sede, FechaProceso, HoraProceso, UsuarioRegistro, IdEmpresa)
    VALUES (codigoColaboradorPS, tipoDocumento, numeroDocumento, nombres, apellidoPaterno, apellidoMaterno, correo, contrasenia, fechaIngreso, salario, puestoLaboral, ubicacionGeografica, sede, CURDATE(), CURTIME(), Usuario, idEmpresa);
	
	SELECT LAST_INSERT_ID() as IdColaborador;
	
END$$
DELIMITER ;