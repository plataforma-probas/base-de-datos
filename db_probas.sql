CREATE DATABASE db_probas;

-- -----------------------------------------------------
-- Perfil de Usuario
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblPerfil (
  IdPerfil INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  Codigo CHAR(3) NOT NULL,
  Descripcion VARCHAR(100) NOT NULL,
  Estado TINYINT(1) NOT NULL DEFAULT 1,
  MarcaBaja TINYINT(1) NOT NULL DEFAULT 0,
  FechaProceso DATE NOT NULL,
  HoraProceso TIME NOT NULL,
  UsuarioRegistro VARCHAR(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Persona 
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblPersona (
  IdPersona INT(11) NOT NULL PRIMARY KEY,
  TipoDocumento TINYINT(1) NOT NULL,
  NumeroDocumento VARCHAR(15) NOT NULL,
  ApellidoPaterno VARCHAR(20) NOT NULL,
  ApellidoMaterno VARCHAR(20) NOT NULL,
  Nombres VARCHAR(30) NOT NULL,
  Direccion VARCHAR(100) DEFAULT NULL,
  Correo VARCHAR(60) DEFAULT NULL,
  NumeroContacto VARCHAR(15) NOT NULL,
  MarcaBaja TINYINT(1) NOT NULL DEFAULT 0,
  FechaProceso DATE NOT NULL,
  HoraProceso TIME NOT NULL,
  UsuarioRegistro VARCHAR(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Usuario
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblUsuario (
  IdUsuario INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  Usuario VARCHAR(50) NOT NULL,
  Contrasenia VARBINARY(8000) NOT NULL,
  Estado INT NOT NULL DEFAULT 1,
  MarcaBaja TINYINT(1) NOT NULL DEFAULT 0,
  FechaProceso DATE NOT NULL,
  HoraProceso TIME NOT NULL,
  UsuarioRegistro VARCHAR(20) NOT NULL,
  IdPersona INT(11) NOT NULL,
  IdPerfil INT(11) NOT NULL,
  FOREIGN KEY (IdPersona) REFERENCES tblPersona (IdPersona),
  FOREIGN KEY (IdPerfil) REFERENCES tblPerfil (IdPerfil)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Parametros
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblParametros (
  Prefijo INT NOT NULL,
  Correlativo SMALLINT NULL,
  Tipo SMALLINT NULL,
  Descripcion1 VARCHAR(255) NULL,
  Descripcion2 VARCHAR(255) NULL,
  Estado SMALLINT NULL,
  UsuarioRegistro VARCHAR(20) NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Empresa / Tienda
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblEmpresa (
  IdEmpresa INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  CodigoEmpresaPS INT(11) DEFAULT NULL,
  RazonSocial VARCHAR(100) NOT NULL,
  Ruc VARCHAR(11) DEFAULT NULL,
  Direccion VARCHAR(100) DEFAULT NULL,
  Representante1 VARCHAR(100) DEFAULT NULL,
  Area1 VARCHAR(100) DEFAULT NULL,
  Representante2 VARCHAR(100) DEFAULT NULL,
  Area2 VARCHAR(100) DEFAULT NULL,
  MarcaBaja TINYINT(1) NOT NULL DEFAULT 0,
  FechaProceso DATE NOT NULL,
  HoraProceso TIME NOT NULL,
  UsuarioRegistro VARCHAR(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Colaborador / Cliente
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblColaborador (
  IdColaborador INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  CodigoColaboradorPS INT(11) DEFAULT NULL,
  TipoDocumento INT(1) NOT NULL,
  NumeroDocumento VARCHAR(15) NOT NULL,
  ApellidoPaterno VARCHAR(40) NOT NULL,
  ApellidoMaterno VARCHAR(40) NULL,
  Nombres VARCHAR(50) NOT NULL,
  Correo VARCHAR(60) NOT NULL,
  Contrasenia VARCHAR(50) NOT NULL,
  FechaIngreso DATE NULL,
  Salario INT(1) NULL,
  PuestoLaboral VARCHAR(50) NULL,
  UbicacionGeografica VARCHAR(50) NULL,
  Sede VARCHAR(50) NULL,
  MarcaBaja TINYINT(1) NOT NULL DEFAULT 0,
  FechaProceso DATE NOT NULL,
  HoraProceso TIME NOT NULL,
  UsuarioRegistro VARCHAR(20) NOT NULL,
  IdEmpresa INT(11) NOT NULL,
  FOREIGN KEY (IdEmpresa) REFERENCES tblEmpresa (IdEmpresa)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;