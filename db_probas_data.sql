-- ------------------------
-- Insert para tabla perfil
-- ------------------------
INSERT INTO tblperfil (Codigo, Descripcion, FechaProceso, HoraProceso, UsuarioRegistro) 
VALUES ('ADM', 'Administrador', CURDATE(), CURTIME(), 'admin');

-- -------------------------
-- Insert para tabla persona
-- -------------------------
INSERT INTO tblpersona (IdPersona, TipoDocumento, NumeroDocumento, ApellidoPaterno, ApellidoMaterno, Nombres, Direccion, Correo, NumeroContacto, FechaProceso, HoraProceso, UsuarioRegistro) 
VALUES (70752618, 1, '70752618', '', '', 'Administrador Probas', 'Lima', 'admin@probas.com', '990148740', CURDATE(), CURTIME(), 'admin');

-- -------------------------
-- Insert para tabla usuario
-- -------------------------
INSERT INTO tblusuario (Usuario, Contrasenia, IdPersona, IdPerfil, FechaProceso, HoraProceso, UsuarioRegistro) 
VALUES ('admin', AES_ENCRYPT('ProbasAdmin2020@','AES'), 70752618, 1, CURDATE(), CURTIME(), 'admin');

-- ----------------------------
-- Insert para tabla parametros
-- ----------------------------
-- Tipos de documento 
INSERT INTO tblParametros (Prefijo, Correlativo, Descripcion1, Estado)
VALUES (1, 0, 'Tipos de Documento', 1);
INSERT INTO tblParametros (Prefijo, Correlativo, Descripcion1, Estado)
VALUES (1, 1, 'DNI', 1);
INSERT INTO tblParametros (Prefijo, Correlativo, Descripcion1, Estado)
VALUES (1, 2, 'Carnet de Extranjeria', 1);
INSERT INTO tblParametros (Prefijo, Correlativo, Descripcion1, Estado)
VALUES (1, 3, 'Pasaporte', 1);
INSERT INTO tblParametros (Prefijo, Correlativo, Descripcion1, Estado)
VALUES (1, 4, 'Otros', 1);
-- Rangos de Sueldo
INSERT INTO tblParametros (Prefijo, Correlativo, Descripcion1, Estado)
VALUES (2, 0, 'Rangos de Sueldos', 1);
INSERT INTO tblParametros (Prefijo, Correlativo, Descripcion1, Estado)
VALUES (2, 1, 'Menor a 1000', 1);
INSERT INTO tblParametros (Prefijo, Correlativo, Descripcion1, Estado)
VALUES (2, 2, 'Entre 1000 y 3000', 1);
INSERT INTO tblParametros (Prefijo, Correlativo, Descripcion1, Estado)
VALUES (2, 3, 'Mayor a 3000', 1);